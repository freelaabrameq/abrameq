﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(CameraPath))]
public class CameraPathEditor : Editor
{
    [SerializeField]
    private CameraPath _target;

    private int _currentBezierPoint;
    private int _currentCheckPoint;

    private Transform _focusedObject;
    private float _speed;

    void OnEnable()
    {
        _target = (CameraPath)target;

        Tools.current = Tool.None;
        //_target.transform.hideFlags = HideFlags.None;

        _currentBezierPoint = -1;
        _currentCheckPoint = -1;
    }
    public void ReordenaLista()
    {
        _target.bezierPoint.Sort((x, y) =>
        {
            if (x.id < y.id)
                return -1;
            else
                return 1;
        }
        );

        for (int i = 0; i < _target.bezierPoint.Count; i++)
        {
            _target.bezierPoint[i].id = i;
        }
    }
    public override void OnInspectorGUI()
    {
        //		DrawDefaultInspector(); 

        EditorGUILayout.Space();

        _target.trnCamera = (Transform)EditorGUILayout.ObjectField("Travelling", _target.trnCamera, typeof(Transform), true);
        _target.orbit = (Orbit)EditorGUILayout.ObjectField("Orbit", _target.orbit, typeof(Orbit), true);

        EditorGUILayout.Space();

        DrawLine();

        if (_target.bezierPoint != null)
        {
            for (int i = 0; i < _target.bezierPoint.Count; i++)
            {
                if (_target.bezierPoint[i].id < 0)
                    _target.bezierPoint[i].id = i;

                var focused = _currentBezierPoint == i;

                GUILayout.BeginHorizontal();
                {
                    if (focused)
                        GUI.color = Color.yellow;
                    

                    GUILayout.Label(i.ToString(), GUILayout.MaxWidth(25));
                    EditorGUILayout.Vector3Field("", _target.bezierPoint[i].wayPoint);

                    _target.bezierPoint[i].currentWayPoint = _target.bezierPoint[i].wayPoint + _target.transform.position;
                    _target.bezierPoint[i].currentPosCP = _target.bezierPoint[i].posControlPoint + _target.transform.position;
                    _target.bezierPoint[i].currentPreCP = _target.bezierPoint[i].preControlPoint + _target.transform.position;

                    if (focused)
                        GUI.enabled = false;

                    if (GUILayout.Button("="))
                    {
                        _currentBezierPoint = i;
                        Repaint();
                        
                    }

                    GUI.enabled = i < _target.bezierPoint.Count - 1;

                    if (GUILayout.Button("\\/"))
                    {
                        _target.bezierPoint[i].id++;
                        _currentBezierPoint = _target.bezierPoint[i].id;

                        ReordenaLista();
                    }
                    GUI.enabled = i > 0;
                    if (GUILayout.Button("/\\"))
                    {
                        _target.bezierPoint[i].id--;
                        _currentBezierPoint = _target.bezierPoint[i].id;

                        ReordenaLista();
                    }

                    //if (focused)
                    //{
                        GUI.enabled = true;
                        GUI.color = Color.white;
                    //}
                }
                GUILayout.EndHorizontal();
            }
        }

        DrawLine();

        EditorGUILayout.Space();

        EditorGUILayout.LabelField("BEZIER POINT");
        EditorGUILayout.BeginVertical();

        EditorGUILayout.BeginHorizontal();
        _focusedObject = (Transform)EditorGUILayout.ObjectField("Target Point", _focusedObject, typeof(Transform), true);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        _speed = EditorGUILayout.FloatField("Speed Travel", _speed);
        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("ADD", GUILayout.MinWidth(100)))
        {
            AddBezierPoint();
        }

        EditorGUILayout.EndVertical();

        if (GUILayout.Button("CLEAR BEZIER POINTS"))
        {
            _currentBezierPoint = -1;
            _currentCheckPoint = -1;
            _target.bezierPoint.Clear();
            _target.checkPoint.Clear();
        }

        EditorGUILayout.Space();
        DrawLine();

        EditorGUILayout.LabelField("BEZIER POINT");

        if (_currentBezierPoint > -1)
        {
            if (_currentBezierPoint < _target.bezierPoint.Count)
            {
                _target.bezierPoint[_currentBezierPoint].target = (Transform)EditorGUILayout.ObjectField("Target Point", _target.bezierPoint[_currentBezierPoint].target, typeof(Transform), true);
            }

            _target.bezierPoint[_currentBezierPoint].speed = EditorGUILayout.FloatField("Speed Travel", _target.bezierPoint[_currentBezierPoint].speed);

            _target.bezierPoint[_currentBezierPoint].orbitObject = (Transform)EditorGUILayout.ObjectField("Target To Orbit", _target.bezierPoint[_currentBezierPoint].orbitObject, typeof(Transform), true);

            _target.bezierPoint[_currentBezierPoint].blockOrbit = EditorGUILayout.Toggle("Block Orbit", _target.bezierPoint[_currentBezierPoint].blockOrbit);

            _target.bezierPoint[_currentBezierPoint].objectId = EditorGUILayout.IntField("Object ID", _target.bezierPoint[_currentBezierPoint].objectId);

            EditorGUILayout.Space();

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("TEST CAMERA START"))
                {
                    _target.trnCamera.transform.position = _target.bezierPoint[_currentBezierPoint].currentWayPoint;
                    if (_currentBezierPoint < _target.bezierPoint.Count - 1)
                        _target.trnCamera.transform.LookAt(_target.bezierPoint[_currentBezierPoint].target);
                    else
                        _target.trnCamera.transform.LookAt(_target.bezierPoint[_currentBezierPoint - 1].target);
                }
                if (_currentBezierPoint < _target.bezierPoint.Count - 1)
                    if (GUILayout.Button("TEST CAMERA END"))
                    {
                        _target.trnCamera.transform.position = _target.bezierPoint[_currentBezierPoint + 1].currentWayPoint;
                        _target.trnCamera.transform.LookAt(_target.bezierPoint[_currentBezierPoint].target);
                    }

            }
            GUILayout.EndHorizontal();
            EditorGUILayout.Space();

            _target.bezierPoint[_currentBezierPoint].wayPoint = EditorGUILayout.Vector3Field("Point: ", _target.bezierPoint[_currentBezierPoint].wayPoint);
            _target.bezierPoint[_currentBezierPoint].preControlPoint = EditorGUILayout.Vector3Field("PrePoint: ", _target.bezierPoint[_currentBezierPoint].preControlPoint);

            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Igualar Pre:");
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Igualar X"))
                {
                    _target.bezierPoint[_currentBezierPoint].preControlPoint.x = _target.bezierPoint[_currentBezierPoint].wayPoint.x;
                }
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Igualar Y"))
                {
                    _target.bezierPoint[_currentBezierPoint].preControlPoint.y = _target.bezierPoint[_currentBezierPoint].wayPoint.y;
                }
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Igualar Z"))
                {
                    _target.bezierPoint[_currentBezierPoint].preControlPoint.z = _target.bezierPoint[_currentBezierPoint].wayPoint.z;
                }
            }
            GUILayout.EndHorizontal();

            _target.bezierPoint[_currentBezierPoint].posControlPoint = EditorGUILayout.Vector3Field("PosPoint: ", _target.bezierPoint[_currentBezierPoint].posControlPoint);

            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Igualar Pos:");
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Igualar X"))
                {
                    _target.bezierPoint[_currentBezierPoint].posControlPoint.x = _target.bezierPoint[_currentBezierPoint+1].wayPoint.x;
                }
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Igualar Y"))
                {
                    _target.bezierPoint[_currentBezierPoint].posControlPoint.y = _target.bezierPoint[_currentBezierPoint+1].wayPoint.y;
                }
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Igualar Z"))
                {
                    _target.bezierPoint[_currentBezierPoint].posControlPoint.z = _target.bezierPoint[_currentBezierPoint+1].wayPoint.z;
                }
            }
            GUILayout.EndHorizontal();

            if (GUILayout.Button("Igualar Tudo"))
            {
                var nextWayPoint = _currentBezierPoint + 1;

                if (nextWayPoint > _target.bezierPoint.Count - 1)
                {
                    nextWayPoint = 0;
                }

                _target.bezierPoint[_currentBezierPoint].preControlPoint = _target.bezierPoint[_currentBezierPoint].wayPoint;
                _target.bezierPoint[_currentBezierPoint].posControlPoint = _target.bezierPoint[nextWayPoint].wayPoint;
            }

            EditorGUILayout.Space();

            if (GUILayout.Button("DELETE BEZIER"))
            {
                _target.bezierPoint.RemoveAt(_currentBezierPoint);
                _target.checkPoint.RemoveAt(_currentBezierPoint);

                _currentBezierPoint = -1;

                return;
            }

            EditorGUILayout.Space();

            if (_currentBezierPoint < _target.bezierPoint.Count - 1)
            {
                EditorGUILayout.LabelField("PRIMITIVE LINES");

                if (GUILayout.Button("Linear"))
                {
                    float d;
                    d = Vector3.Distance(_target.bezierPoint[_currentBezierPoint].wayPoint, _target.bezierPoint[_currentBezierPoint + 1].wayPoint);
                    d = Mathf.Abs(d) * 0.4f;

                    _target.bezierPoint[_currentBezierPoint].preControlPoint = _target.bezierPoint[_currentBezierPoint].wayPoint;
                    _target.bezierPoint[_currentBezierPoint].posControlPoint = _target.bezierPoint[_currentBezierPoint + 1].wayPoint;

                    _target.bezierPoint[_currentBezierPoint].preControlPoint.x = _target.bezierPoint[_currentBezierPoint].wayPoint.x + d;
                    _target.bezierPoint[_currentBezierPoint].posControlPoint.x = _target.bezierPoint[_currentBezierPoint + 1].wayPoint.x - d;
                }

                if (GUILayout.Button("Sinusoid"))
                {
                    float d;
                    d = Vector3.Distance(_target.bezierPoint[_currentBezierPoint].wayPoint, _target.bezierPoint[_currentBezierPoint + 1].wayPoint);
                    d = Mathf.Abs(d) * 0.5f;

                    _target.bezierPoint[_currentBezierPoint].preControlPoint = _target.bezierPoint[_currentBezierPoint].wayPoint;
                    _target.bezierPoint[_currentBezierPoint].posControlPoint = _target.bezierPoint[_currentBezierPoint + 1].wayPoint;

                    _target.bezierPoint[_currentBezierPoint].preControlPoint.x = _target.bezierPoint[_currentBezierPoint].wayPoint.x + d;
                    _target.bezierPoint[_currentBezierPoint].posControlPoint.x = _target.bezierPoint[_currentBezierPoint].wayPoint.x + d;

                    _target.bezierPoint[_currentBezierPoint].preControlPoint.z = _target.bezierPoint[_currentBezierPoint].wayPoint.z + 5;
                    _target.bezierPoint[_currentBezierPoint].posControlPoint.z = _target.bezierPoint[_currentBezierPoint].wayPoint.z - 5;
                }

                if (GUILayout.Button("Parabola"))
                {
                    float d;
                    d = Vector3.Distance(_target.bezierPoint[_currentBezierPoint].wayPoint, _target.bezierPoint[_currentBezierPoint + 1].wayPoint);
                    d = Mathf.Abs(d) * 0.6f;

                    _target.bezierPoint[_currentBezierPoint].preControlPoint = _target.bezierPoint[_currentBezierPoint].wayPoint;
                    _target.bezierPoint[_currentBezierPoint].posControlPoint = _target.bezierPoint[_currentBezierPoint + 1].wayPoint;

                    _target.bezierPoint[_currentBezierPoint].preControlPoint.x = _target.bezierPoint[_currentBezierPoint].wayPoint.x + d;
                    _target.bezierPoint[_currentBezierPoint].posControlPoint.x = _target.bezierPoint[_currentBezierPoint + 1].wayPoint.x - d;

                    _target.bezierPoint[_currentBezierPoint].preControlPoint.z = _target.bezierPoint[_currentBezierPoint].wayPoint.z + 5;
                    _target.bezierPoint[_currentBezierPoint].posControlPoint.z = _target.bezierPoint[_currentBezierPoint].wayPoint.z + 5;
                }
            }
        }
        else
        {
            EditorGUILayout.LabelField("No bezier point selected.");
        }

        DrawLine();

        if (_currentCheckPoint > -1)
        {
            _target.checkPoint[_currentCheckPoint] = EditorGUILayout.Vector3Field("CHECK POINT", _target.checkPoint[_currentCheckPoint]);

        }
        else
        {
            EditorGUILayout.LabelField("CHECK POINT");
            EditorGUILayout.LabelField("No check point selected.");
        }

        DrawLine();

        if( GUI.changed ){
        EditorUtility.SetDirty(_target);
        }
    }

    void OnSceneGUI()
    {
        Handles.color = Color.white;
        Handles.CubeCap(0, _target.transform.position, _target.transform.rotation, .5F);

        if (_target.bezierPoint != null && _target.bezierPoint.Count > 1)
        {

            for (int i = 0; i < _target.bezierPoint.Count; i++)
            {
                if (i == _currentBezierPoint)
                {
                    Handles.color = Color.yellow;

                    _target.bezierPoint[i].wayPoint = Handles.PositionHandle(_target.bezierPoint[i].wayPoint, Quaternion.identity);

                    Handles.SphereCap(0, _target.bezierPoint[i].wayPoint + _target.transform.position, Quaternion.identity, 0.5f);

                }
                else
                {

                    Handles.color = Color.red;
                    float width = 0.5f;//HandleUtility.GetHandleSize(Vector3.zero) * 0.15f;

                    if (Handles.Button(_target.bezierPoint[i].wayPoint + _target.transform.position, Quaternion.identity, width, 0.5f, Handles.SphereCap))
                    {
                        _currentBezierPoint = i;
                        Repaint();
                    }
                }
            }

            #region >> CHECK POINT

            //if (_target.checkPoint != null)
            //{
            //    for (int i = 0; i < _target.checkPoint.Count; i++)
            //    {
            //        float width = 0.5f; // HandleUtility.GetHandleSize(Vector3.zero) * 0.15f;

            //        if (i == _currentCheckPoint)
            //        {
            //            _target.checkPoint[i] = Handles.PositionHandle(_target.checkPoint[i], Quaternion.identity);

            //            Handles.color = Color.green;
            //            Handles.DrawLine(_target.checkPoint[i], _target.bezierPoint[i].wayPoint);
            //            Handles.SphereCap(0, _target.checkPoint[i], Quaternion.identity, 0.35f);

            //        }
            //        else
            //        {

            //            Handles.color = Color.yellow;
            //            if (Handles.Button(_target.checkPoint[i], Quaternion.identity, width * 0.75f, 3, Handles.DrawSphere))
            //            {
            //                _currentCheckPoint = i;
            //                Repaint();
            //            }

            //            Handles.color = Color.white;
            //            Handles.DrawLine(_target.checkPoint[i], _target.bezierPoint[i].wayPoint);
            //        }
            //    }
            //}
            #endregion

            #region >> TARGETs

            if (_currentBezierPoint < _target.bezierPoint.Count - 1 && _currentBezierPoint > -1)
            {
                if (_target.bezierPoint[_currentBezierPoint].target == null)
                    _target.bezierPoint[_currentBezierPoint].target = _target.transform;

                _target.bezierPoint[_currentBezierPoint].target.position = Handles.PositionHandle(_target.bezierPoint[_currentBezierPoint].target.position, Quaternion.identity);

                Handles.color = Color.green;
                Handles.SphereCap(0, _target.bezierPoint[_currentBezierPoint].target.position, Quaternion.identity, 0.35f);
            }

            #endregion

            for (int i = 0; i < _target.bezierPoint.Count; i++)
            {
                int nexWayPoint = i + 1;

                if (i == _target.bezierPoint.Count - 1)
                {
                    nexWayPoint = 0;
                }

                Color colorLine = Color.red;

                if (i == _currentBezierPoint)
                {
                    colorLine = Color.yellow;
                }


                Handles.DrawBezier(_target.bezierPoint[i].currentWayPoint,
                    _target.bezierPoint[nexWayPoint].currentWayPoint,
                    _target.bezierPoint[i].preControlPoint + _target.transform.position,
                    _target.bezierPoint[i].posControlPoint + _target.transform.position,
                    colorLine, null, 4);

            }

            Handles.color = Color.white;

            if (_currentBezierPoint > -1)
            {
                if (_target.loopPath && _currentBezierPoint == _target.bezierPoint.Count - 1)
                {
                    Handles.DrawLine(_target.bezierPoint[_currentBezierPoint].currentWayPoint, _target.bezierPoint[_currentBezierPoint].preControlPoint);
                    Handles.DrawLine(_target.bezierPoint[0].currentWayPoint, _target.bezierPoint[_currentBezierPoint].posControlPoint);

                    _target.bezierPoint[_currentBezierPoint].preControlPoint =
                        PositionHandle(_target.bezierPoint[_currentBezierPoint].preControlPoint,
                            Quaternion.identity);

                    _target.bezierPoint[_currentBezierPoint].posControlPoint =
                        PositionHandle(_target.bezierPoint[_currentBezierPoint].posControlPoint,
                            Quaternion.identity);

                }
                else if (_currentBezierPoint < _target.bezierPoint.Count - 1)
                {

                    Handles.DrawLine(_target.bezierPoint[_currentBezierPoint].currentWayPoint, _target.bezierPoint[_currentBezierPoint].preControlPoint);
                    Handles.DrawLine(_target.bezierPoint[_currentBezierPoint + 1].currentWayPoint, _target.bezierPoint[_currentBezierPoint].posControlPoint);

                    _target.bezierPoint[_currentBezierPoint].preControlPoint =
                        PositionHandle(_target.bezierPoint[_currentBezierPoint].preControlPoint,
                            Quaternion.identity);

                    _target.bezierPoint[_currentBezierPoint].posControlPoint =
                        PositionHandle(_target.bezierPoint[_currentBezierPoint].posControlPoint,
                            Quaternion.identity);
                }
            }

        }

        EditorUtility.SetDirty(_target);
    }

    public static Vector3 PositionHandle(Vector3 position, Quaternion rotation)
    {
        float handleSize = HandleUtility.GetHandleSize(position) * 0.5f;
        Vector3 snapMove = new Vector3(EditorPrefs.GetFloat("MoveSnapX"), EditorPrefs.GetFloat("MoveSnapY"), EditorPrefs.GetFloat("MoveSnapZ"));

        Color color = Handles.color;

        Handles.color = new Color(255f / 255f, 165f / 255f, 0f, 1f);
        position = Handles.Slider(position, rotation * Vector3.right, handleSize, new Handles.DrawCapFunction(Handles.ArrowCap), snapMove.x);
        Handles.color = Color.green;
        position = Handles.Slider(position, rotation * Vector3.up, handleSize, new Handles.DrawCapFunction(Handles.ArrowCap), snapMove.y);
        Handles.color = Color.magenta;
        position = Handles.Slider(position, rotation * Vector3.forward, handleSize, new Handles.DrawCapFunction(Handles.ArrowCap), snapMove.z);

        Handles.color = Color.white;
        position = Handles.FreeMoveHandle(position, rotation, handleSize * 0.15f, snapMove, new Handles.DrawCapFunction(Handles.RectangleCap));

        Handles.color = color;

        return position;
    }

    [MenuItem("frOTUS/Bezier Path/New Path")]
    public static void Init()
    {
        GameObject GOBezierPath = (GameObject)Instantiate(new GameObject(), Vector3.zero, Quaternion.identity) as GameObject;
        GOBezierPath.name = "Bezier Path";
        var cp = GOBezierPath.AddComponent<CameraPath>();

        cp.bezierPoint = new List<BezierPoint>();
        cp.checkPoint = new List<Vector3>();
    }

    private void DrawLine()
    {
        EditorGUILayout.Space();
        GUILayout.Box("", new GUILayoutOption[] { GUILayout.ExpandWidth(true), GUILayout.Height(1) });
        EditorGUILayout.Space();
    }

    private void AddBezierPoint()
    {
        if (_target.bezierPoint == null || _target.bezierPoint.Count == 0)
        {
            _target.bezierPoint = new List<BezierPoint>();

            try
            {
                NewBezierPoint();
            }
            catch
            {
                Debug.LogWarning("[ERROR]: You must set TARGER POINT");
                return;
            }
        }

        int aux = _target.bezierPoint.Count - 1;
        NewBezierPoint();


        _target.bezierPoint[aux].preControlPoint = _target.bezierPoint[aux].wayPoint + _target.transform.localPosition;
        _target.bezierPoint[aux].posControlPoint = _target.bezierPoint[aux + 1].wayPoint +_target.transform.localPosition;

        float d;
        d = Vector3.Distance(_target.bezierPoint[aux].wayPoint, _target.bezierPoint[aux + 1].wayPoint);
        d = Mathf.Abs(d) * 0.4f;

        _target.bezierPoint[aux].preControlPoint.x = _target.bezierPoint[aux].wayPoint.x + d;
        _target.bezierPoint[aux].posControlPoint.x = _target.bezierPoint[aux + 1].wayPoint.x - d;
    }

    private void NewBezierPoint()
    {
        BezierPoint bezierPointAux;

        bezierPointAux = new BezierPoint();
        bezierPointAux.wayPoint = _focusedObject.position;
        bezierPointAux.wayPoint.z -= 10;

        bezierPointAux.speed = _speed;
        bezierPointAux.target = _focusedObject;

        if (_currentBezierPoint > -1 && _currentBezierPoint < _target.bezierPoint.Count - 1)
        {
            _target.bezierPoint.Insert(_currentBezierPoint + 1, bezierPointAux);
        }
        else
        {
            _target.bezierPoint.Add(bezierPointAux);
        }

        _target.checkPoint.Clear();

        for (int i = 0; i < _target.bezierPoint.Count; i++)
        {
            _target.bezierPoint[i].id = i;
            NewCheckPoint(_target.bezierPoint[i]);
        }

        _focusedObject = null;
        _speed = 1;
    }

    private void NewCheckPoint(BezierPoint bzp)
    {
        Vector3 auxV3;

        _target.checkPoint.Add(bzp.wayPoint);

        auxV3 = bzp.wayPoint;
        auxV3.z = bzp.target.position.z;

        _target.checkPoint[bzp.id] = auxV3;
    }



}