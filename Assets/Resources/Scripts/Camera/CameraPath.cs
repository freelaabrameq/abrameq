﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraPath : MonoBehaviour
{
    //	public UILabel nameMachine;

    public List<BezierPoint> bezierPoint;
    public List<Vector3> checkPoint;
    //public List<string> names;

    public Transform target;

    public Transform trnCamera;
    public Transform player;
    public Transform point;

    // Movement speed in units/sec.
    public float currentTime;
    private float journeyLength;

    public bool loopPath;

    public int currentPath;

    private int _nextPath;

    private float t;

    private float distPlayerCK0;
    private float distPlayerCK1;

    public float smooth = 5.0f;

    public Orbit orbit;

    //private Vector3 prevPos;
    //private Quaternion prevRot;

    private TelaExecutaPasseio _telaPasseio;

    private bool _wasInitialized, _orbiting;

    public Vector3 aux;

    private static int S_currentPath;
    private static float S_currentTime;

    public void Init(TelaExecutaPasseio telaPasseio)
    {
        _telaPasseio = telaPasseio;

        currentPath = S_currentPath;
        _nextPath = currentPath + 1;

        currentTime = S_currentTime;
        journeyLength = Vector3.Distance(bezierPoint[currentPath].wayPoint, bezierPoint[_nextPath].wayPoint);

        orbit.target = bezierPoint[currentPath].target;

        float distCovered = (currentTime) * bezierPoint[currentPath].speed;
        t = distCovered / journeyLength;

        aux = CalculateBezierPoint(Mathf.Abs(t), bezierPoint[currentPath].currentWayPoint, bezierPoint[currentPath].currentPreCP, bezierPoint[currentPath].currentPosCP, bezierPoint[_nextPath].currentWayPoint);

        trnCamera.position = bezierPoint[currentPath].currentWayPoint;
        trnCamera.LookAt(bezierPoint[currentPath].target);

        _orbiting = false;
        orbit.canOrbit = false;

        _telaPasseio.EnableInfo(bezierPoint[currentPath].objectId);

        _wasInitialized = true;
    }
    public void SaveState()
    {
        S_currentPath = this.currentPath; 
        S_currentTime = this.currentTime;
    }
    public void ClearState()
    {
        S_currentPath = 0;
        S_currentTime = 0;
    }
    public bool Orbit(bool block)
    {
        if (!_orbiting)
        {
            _orbiting = orbit.Init(bezierPoint[currentPath].orbitObject ? bezierPoint[currentPath].orbitObject : bezierPoint[currentPath].target);
        }
        else
        {
            _orbiting = false;
        }

        orbit.canOrbit = !orbit.canOrbit;

        if (_orbiting && block)
        {
            orbit.canOrbit = false;

            var next = currentPath + 1 < bezierPoint.Count ? currentPath + 1 : 0;

            orbit.transform.position = bezierPoint[next].currentWayPoint;
            orbit.transform.LookAt(bezierPoint[currentPath].target);
        }

        return _orbiting;
    }
    public void MudaPontoPasseio(bool avanca)
    {
        if (_orbiting)
            return;

        if (avanca)
        {
            currentPath++;
            _nextPath++;

            if (currentPath == bezierPoint.Count - 1)
            {
                _nextPath = 0;
            }
            if (currentPath == bezierPoint.Count)
            {
                currentPath = 0;
            }
        }
        else
        {
            currentPath--;
            _nextPath--;

            if (_nextPath == -1)
            {
                _nextPath = bezierPoint.Count - 1;
            }
            if (currentPath == -1)
            {
                currentPath = bezierPoint.Count - 1;
            }
        }

        _telaPasseio.EnableInfo(bezierPoint[currentPath].objectId);

        orbit.target = bezierPoint[currentPath].target;
        journeyLength = Vector3.Distance(bezierPoint[currentPath].wayPoint, bezierPoint[_nextPath].wayPoint);
        currentTime = 0;
    }
    void Update()
    {
        if (!_wasInitialized)
            return;

        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            MudaPontoPasseio(true);
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            MudaPontoPasseio(false);
        }

        if (_orbiting)
            return;

        if (trnCamera != null)
            trnCamera.position = Vector3.Lerp(trnCamera.position, aux, Time.deltaTime * 5);

        if (currentPath != bezierPoint.Count)
            SmoothLookAt();
    }


    void LateUpdate()
    {
        if (!_wasInitialized)
            return;

        if (_orbiting)
            return;

        if (bezierPoint.Count > 0 && trnCamera != null)
        {
            if (t > 1)
            {
                currentPath++;
                _nextPath++;

                if (currentPath == bezierPoint.Count - 1)
                {
                    _nextPath = 0;
                }
                if (currentPath == bezierPoint.Count)
                {
                    currentPath = 0;
                }

                _telaPasseio.EnableInfo(bezierPoint[currentPath].objectId);

                orbit.target = bezierPoint[currentPath].target;
                journeyLength = Vector3.Distance(bezierPoint[currentPath].wayPoint, bezierPoint[_nextPath].wayPoint);
                currentTime = 0;
            }

            float distCovered = (currentTime) * bezierPoint[currentPath].speed;
            t = distCovered / journeyLength;

            aux = CalculateBezierPoint(Mathf.Abs(t), bezierPoint[currentPath].currentWayPoint, bezierPoint[currentPath].currentPreCP, bezierPoint[currentPath].currentPosCP, bezierPoint[_nextPath].currentWayPoint);

            currentTime += Time.deltaTime;
        }
    }

    private Vector3 CalculateBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector3 p = uuu * p0; 	//first term
        p += 3 * uu * t * p1; 	//second term
        p += 3 * u * tt * p2; 	//third term
        p += ttt * p3; 			//fourth term

        return p;
    }

    private void SmoothLookAt()
    {
        // Create a vector from the camera towards the player.
        Vector3 relPlayerPosition = bezierPoint[currentPath].target.position - trnCamera.position;

        // Create a rotation based on the relative position of the player being the forward vector.
        Quaternion lookAtRotation = Quaternion.LookRotation(relPlayerPosition, Vector3.up);

        // Lerp the camera's rotation between it's current rotation and the rotation that looks at the player.
        trnCamera.rotation = Quaternion.Lerp(trnCamera.rotation, lookAtRotation, smooth * Time.deltaTime);
    }

}