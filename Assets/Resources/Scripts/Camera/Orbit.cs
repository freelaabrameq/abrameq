﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Orbit : MonoBehaviour 
{ 
    public Transform target;
    public bool isFixed;

    public float distance 	= 8;
    public float distanceMin = 5F;
    public float distanceMax = 20F;

    public float xSpeed 	= 1.0f;
    public float ySpeed 	= 1.0f;
 	public float zSpeed 	= 5.0f;
	
    public float yMinLimit 	= -20f;
    public float yMaxLimit 	= 80f;

    private float x = 0.0f;
    private float y = 0.0f;
 
	public bool canOrbit;

    public float mouseWheelSpeed = 500F;
    public float perspectiveZoomSpeed = 1;        // The rate of change of the field of view in perspective mode.

    private float _prevTouchDeltaMag, _tempoDeToque, _minDistanceDefault, _maxDistanceDefault;
    private Vector2 _initTouchZero, _initTouchOne;

    //public Slider slider;
    //public Text txtSlider;

	// Use this for initialization
	void Start () 
	{
        Vector3 angles = this.transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        _minDistanceDefault = distanceMin;
        _maxDistanceDefault = distanceMax;

        if (this.GetComponent<Rigidbody>())
            this.GetComponent<Rigidbody>().freezeRotation = true;

        //slider.value = perspectiveZoomSpeed;

        //ChangeSpeed();
	}
    public bool Init(Transform focusTarget)
    {
        if (focusTarget)
        {
            if (isFixed)
            {
                this.transform.position = Vector3.zero;
                this.transform.rotation = Quaternion.identity;
            }

            var distanceDefined = focusTarget.GetComponentInChildren<DistanceMinMax>();

            if (distanceDefined)
            {
                distanceMax = distanceDefined.MaxDistance;
                distanceMin = distanceDefined.MinDistance;
            }
            else
            {
                distanceMin = _minDistanceDefault;
                distanceMax = _maxDistanceDefault;                
            }

            distance = Mathf.Lerp(distanceMin, distanceMax, .5F);

            Vector3 angles = this.transform.eulerAngles;
            x = angles.y;
            y = angles.x;

            target = focusTarget;

            Quaternion rotation = Quaternion.Euler(0, 0, 0);

            RaycastHit hit;
            if (Physics.Linecast(target.position, this.transform.position, out hit))
            {
                distance -= hit.distance * 2;
            }
            Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
            Vector3 position = rotation * negDistance + target.position;

            this.transform.position = position;
            this.transform.rotation = rotation;

            y = ClampAngle(y, yMinLimit, yMaxLimit);

            rotation = Quaternion.Euler(y, x, 0);

            if (Physics.Linecast(target.position, this.transform.position, out hit))
            {
                distance -= hit.distance * 2;
            }
            negDistance = new Vector3(0.0f, 0.0f, -distance);
            position = rotation * negDistance + target.position;

            this.transform.rotation = rotation;
            this.transform.position = position;

            return true;
        }

        return false;
    }
    //public void ChangeSpeed()
    //{
    //    perspectiveZoomSpeed = slider.value;
    //    txtSlider.text = slider.value.ToString("#.00");
    //}
    void Update () 
	{
		if( !canOrbit )
			return;

#if UNITY_STANDALONE //|| UNITY_EDITOR
        //if (Input.GetMouseButton(0))
        //{
        //    if (target)
        //    {
        //        x += Input.GetAxis("Mouse X") * xSpeed * distance * 2;
        //        y -= Input.GetAxis("Mouse Y") * ySpeed * 2;

        //        y = ClampAngle(y, yMinLimit, yMaxLimit);

        //        Quaternion rotation = Quaternion.Euler(y, x, 0);

        //        RaycastHit hit;
        //        if (Physics.Linecast(target.position, this.transform.position, out hit))
        //        {
        //            distance -= hit.distance * 2;
        //        }
        //        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
        //        Vector3 position = rotation * negDistance + target.position;

        //        this.transform.rotation = rotation;
        //        this.transform.position = position;
        //    }
        //}

        distance -= Input.GetAxis("Mouse ScrollWheel") * mouseWheelSpeed * Time.deltaTime;

        Zoom();
#else
        if (Input.touchCount == 2)
        {
            _tempoDeToque = 0;

            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            distance += deltaMagnitudeDiff * perspectiveZoomSpeed * Time.deltaTime;

            Zoom();
        }
        else if (Input.touchCount == 1)
        {
            if (_tempoDeToque < .15F)
            {
                _tempoDeToque += Time.deltaTime;
                return;
            }

            if (target)
            {
                x += Input.GetAxis("Mouse X") * xSpeed * distance;
                y -= Input.GetAxis("Mouse Y") * ySpeed;

                y = ClampAngle(y, yMinLimit, yMaxLimit);

                Quaternion rotation = Quaternion.Euler(y, x, 0);

                RaycastHit hit;
                if (Physics.Linecast(target.position, this.transform.position, out hit))
                {
                    distance -= hit.distance * 2;
                }
                Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
                Vector3 position = rotation * negDistance + target.position;

                this.transform.rotation = rotation;
                this.transform.position = position;
            }
        }
        else
        {
            _tempoDeToque = 0;
        }
#endif
    }
    private void Zoom()
    {
        distance = Mathf.Clamp(distance, distanceMin, distanceMax);

        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
        Vector3 position = this.transform.rotation * negDistance + target.position;

        this.transform.position = position;
    }
    public static float ClampAngle( float angle, float min, float max )
    {
        if (angle < -360F)
            angle += 360F;
        else if (angle > 360F)
            angle -= 360F;
		
        return Mathf.Clamp(angle, min, max);
    }
}