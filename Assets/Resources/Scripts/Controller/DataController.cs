﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using V_Lib;

public class DataController : MonoBehaviour
{
    #region CONST E READONLY
    public const int NoIdiomas = 3;

    public static readonly string[] Idiomas = new string[NoIdiomas] { "pt-BR", "en-US", "es-ES" };

    public List<string> Palavras { get; private set; }
    #endregion

    public DataEmpresa DadosEmpresa { get; private set; }

    public static DataEmpresa DadosEmpresaAtual { get; private set; }

    public List<DataEmpresa> Empresas { get; set; }

    public DataApp DadosApp { get; private set; }

    public List<DataPasseio> passeios { get; private set; }

    public static int IdiomaId { get; private set; }

    public int empresaId;

    public int passeioId { get; private set; }

    public DataItemCatalogo ItemCatalogoAtual { get; private set; }

    public Sprite imgNotFound { get; private set; }

    //public List<CameraPath> passeios { get; private set; }
    [HideInInspector]
    public bool passeioNotFold;

    public void Init()
    {
        Load();

        //SetaIdioma(0);
    }

    public void Save()
    {
        DadosApp.PrepareToSave();

        XmlHelper.CreateXML(Application.dataPath + "/Resources/Data/DataEmpresas/", "Empresa_" + empresaId + ".xml", DadosEmpresa.XmlSerialize<DataEmpresa>());

        for (int i = 0; i < Empresas.Count; i++)
        {
            XmlHelper.CreateXML(Application.dataPath + "/Resources/Data/DataEmpresas/", "Empresa_" + Empresas[i].empresaId + ".xml", Empresas[i].XmlSerialize<DataEmpresa>());
        }

        XmlHelper.CreateXML(Application.dataPath + "/Resources/Data/", "DataApp.xml", DadosApp.XmlSerialize<DataApp>());
        XmlHelper.CreateXML(Application.dataPath + "/Resources/Data/", "DataPasseio.xml", passeios.XmlSerialize<List<DataPasseio>>());
    }
    public void Load()
    {
        //passeios = new List<CameraPath>();

        imgNotFound = Resources.Load("ArtsContents/Generic/Textures/imgNotFound", typeof(Sprite)) as Sprite;

        TextAsset textAsset = Resources.Load("Data/DataEmpresas/Empresa_" + empresaId, typeof(TextAsset)) as TextAsset;

        if (textAsset != null)
        {
            DadosEmpresa = textAsset.text.XmlDeserialize<DataEmpresa>();

            if (DadosEmpresaAtual == null)
                DadosEmpresaAtual = textAsset.text.XmlDeserialize<DataEmpresa>();
        }
        else
        {
            DadosEmpresa = new DataEmpresa();
        }

        textAsset = Resources.Load("Data/DataApp", typeof(TextAsset)) as TextAsset;

        if (textAsset != null)
        {
            DadosApp = textAsset.text.XmlDeserialize<DataApp>();
            DadosApp.Init();
        }
        else
        {
            DadosApp = new DataApp();
            DadosApp.FirstInit();
        }

        //textAsset = Resources.Load("Data/DataPasseio", typeof(TextAsset)) as TextAsset;
        textAsset = Resources.Load("Data/DataPasseios/DataPasseio_" + (empresaId), typeof(TextAsset)) as TextAsset;

        if (textAsset != null)
        {
            passeios = textAsset.text.XmlDeserialize<List<DataPasseio>>();
        }
        else
        {
            passeios = new List<DataPasseio>();
        }

        Empresas = new List<DataEmpresa>();

        for (int i = 0; i < 14; i++)
        {
            textAsset = Resources.Load("Data/DataEmpresas/Empresa_" + (i + 1), typeof(TextAsset)) as TextAsset;

            if (textAsset != null)
            {
                Empresas.Add(textAsset.text.XmlDeserialize<DataEmpresa>());
            }
            else
            {
                Empresas.Add(new DataEmpresa(i));
            }
        }



        ////REMOVE THIS

        //string quebra = "\n";
        //List<string> videos = new List<string>();
        //
        //foreach (var empres in DadosEmpresa.itensCatalogo)
        //{
        //    foreach (var item in empres.videos)
        //    {
        //        videos.Add(item);
        //    }
        //}
        //
        //if (videos.Count > 0)
        //{
        //    List<string> videosUnicos = videos.Distinct().OrderBy(x => x).ToList();
        //    string final = DadosEmpresa.empresaNome + quebra + quebra;
        //        
        //    foreach (var vid in videosUnicos)
        //    {
        //        final += vid + quebra;
        //    }
        //
        //    final += quebra + "TOTAL: " + videosUnicos.Count + quebra + quebra + quebra + quebra;
        //
        //    print(final);
        //}
    }

    public void SetaIdioma(int id)
    {
        //if (DadosEmpresa != null)
        //    DadosEmpresa.SetaIdioma(id);

        IdiomaId = id;
    }
    public Sprite PegaLogo()
    {
        var sprite = Resources.Load("ArtsContents/Texture/Logos/" + DadosEmpresa.logo, typeof(Sprite)) as Sprite;

        if (sprite == null)
            return imgNotFound;
        else
            return sprite;
    }
    public Sprite PegaImagemCatalogo()
    {
        var sprite = Resources.Load("ArtsContents/Texture/IconesMaquinas/" + ItemCatalogoAtual.iconeCatalogo, typeof(Sprite)) as Sprite;

        if (sprite == null)
            return imgNotFound;
        else
            return sprite;
    }
    public Sprite PegaImagemCatalogo(int id)
    {
        var item = DadosEmpresa.itensCatalogo.First(x => x.id == id);

        var sprite = Resources.Load("ArtsContents/Texture/IconesMaquinas/" + item.iconeCatalogo, typeof(Sprite)) as Sprite;

        if (sprite == null)
            return imgNotFound;
        else
            return sprite;
    }
    public void SetaPasseio(int id)
    {
        passeioId = id;
    }

    public void SetaEmpresa(int id)
    {
        empresaId = id;
    }

    public void SetaItemCatalogo(int id)
    {
        //if (DadosEmpresa.itensCatalogo.Count > id)
        //{
            ItemCatalogoAtual = DadosEmpresa.itensCatalogo.First(x => x.id == id);
        //}
        //else
        //{
        //    ItemCatalogoAtual = new DataItemCatalogo();
        //}
    }
}
