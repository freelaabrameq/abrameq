﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[CustomEditor(typeof(DataController))]
public class DataControllerInspector : Editor
{
    private DataController _target;
    private bool _empresasNotFold, _appNotFold, _editandoPalavra;

    private string _palavra;
    private string[] _traducoes;

    private List<bool> _palavrasFold; 

    void OnEnable()
    {
        _target = (DataController)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (_target.DadosEmpresa == null || _target.DadosApp == null)
        {
            _target.Load();
        }
        else if (_target.DadosApp.Palavras == null)
        {
            _target.Load();
        }
        else if (_target.DadosApp.Dicionario == null)
        {
            _target.Load();
        }

        if (_palavrasFold == null)
            _palavrasFold = new List<bool>();

        #region Botões
        GUILayout.BeginHorizontal();
        {
            if (GUILayout.Button("Load"))
                _target.Load();
            if (GUILayout.Button("Save"))
                _target.Save();
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(10);

        //if (GUILayout.Button("teste"))
        //{
        //    _target.Empresas = new List<DataEmpresa>();
        //
        //    for (int i = 0; i < 14; i++)
        //    {
        //        _target.Empresas.Add(new DataEmpresa(i+1));
        //    }
        //
        //    for (int i = 0; i < _target.DadosEmpresa.itensCatalogo.Count; i++)
        //    {
        //        var item = _target.DadosEmpresa.itensCatalogo[i];
        //
        //        for (int j = 0; j < item.fabricantesId.Count; j++)
        //        {
        //            _target.Empresas[item.fabricantesId[j] - 1].itensCatalogo.Add(item);
        //        }
        //    }
        //
        //    Debug.Break();
        //}
        #endregion

        _empresasNotFold = EditorGUILayout.Foldout(_empresasNotFold, "DADOS EMPRESA: ");

        if (_empresasNotFold)
        {
            #region Empresa 0
            GUILayout.BeginVertical("box");
            {
                var data = _target.DadosEmpresa;

                data.geralNotFold = EditorGUILayout.Foldout(data.geralNotFold, "EMPRESA 0 (" + data.empresaNome + "): ");

                if (data.geralNotFold)
                    
                {
                    GUILayout.BeginVertical("box");
                    {
                        #region Empresa 0
                        #region Dados Gerais
                        data.dadosNotFold = EditorGUILayout.Foldout(data.dadosNotFold, "Dados");

                        if (data.dadosNotFold)
                        {
                            GUILayout.Space(5);

                            data.empresaId = EditorGUILayout.IntField("Empresa Id: ", data.empresaId);

                            GUILayout.Space(5);

                            data.empresaNome = EditorGUILayout.TextField("Nome Empresa: ", data.empresaNome);

                            GUILayout.Space(5);

                            data.logo = EditorGUILayout.TextField("Logo: ", data.logo);

                            GUILayout.Space(5);

                            GUILayout.BeginVertical("box");
                            {
                                GUILayout.BeginHorizontal();
                                {
                                    GUILayout.Label("Site 1:");

                                    if (GUILayout.Button("C"))
                                        EditorGUIUtility.systemCopyBuffer = data.site1;
                                    GUILayout.Space(5);
                                    if (GUILayout.Button("P"))
                                        data.site1 = EditorGUIUtility.systemCopyBuffer;
                                }
                                GUILayout.EndHorizontal();

                                data.site1 = EditorGUILayout.TextField(data.site1);

                                GUILayout.BeginHorizontal();
                                {
                                    GUILayout.Label("Site 2:");

                                    if (GUILayout.Button("C"))
                                        EditorGUIUtility.systemCopyBuffer = data.site2;
                                    GUILayout.Space(5);
                                    if (GUILayout.Button("P"))
                                        data.site2 = EditorGUIUtility.systemCopyBuffer;
                                }
                                GUILayout.EndHorizontal();

                                data.site2 = EditorGUILayout.TextField(data.site2);
                            }
                            GUILayout.EndVertical();

                            GUILayout.Space(5);
                        }
                        #endregion
                        #region Itens Catálogo

                        data.itensNotFold = EditorGUILayout.Foldout(data.itensNotFold, "Itens");

                        if (data.itensNotFold)
                        {
                            GUI.backgroundColor = Color.cyan;

                            GUILayout.BeginVertical("box");
                            {
                                if (GUILayout.Button("Add Item"))
                                    data.itensCatalogo.Add(new DataItemCatalogo(data.itensCatalogo.Count));

                                for (int j = 0; j < data.itensCatalogo.Count; j++)
                                {
                                    var item = data.itensCatalogo[j];

                                    item.notFold = EditorGUILayout.Foldout(item.notFold, item.id + " - " + item.nome);

                                    if (item.notFold)
                                    {
                                        GUILayout.BeginVertical("box");
                                        {
                                            GUILayout.Space(5);

                                            item.id = EditorGUILayout.IntField("Item Id:", item.id);

                                            GUILayout.Space(5);

                                            item.nome = EditorGUILayout.TextField("Nome Item:", item.nome);

                                            GUILayout.Space(5);

                                            if (item.nomes == null)
                                            {
                                                item.nomes = new string[3] { item.nome, "", "" };
                                            }

                                            GUILayout.BeginVertical("box");
                                            {
                                                for (int k = 0; k < DataController.NoIdiomas; k++)
                                                {
                                                    GUILayout.BeginHorizontal();
                                                    {
                                                        GUILayout.Label(DataController.Idiomas[k] + ":");

                                                        if (GUILayout.Button("C"))
                                                            EditorGUIUtility.systemCopyBuffer = item.descricao[k];
                                                        GUILayout.Space(5);
                                                        if (GUILayout.Button("P"))
                                                        {
                                                            item.nomes[k] = EditorGUIUtility.systemCopyBuffer;
                                                            if (item.nomes[k].EndsWith("\n"))
                                                            {
                                                                item.nomes[k] = item.nomes[k].Substring(0, item.nomes[k].Length - 1);
                                                            }
                                                        }

                                                    }
                                                    GUILayout.EndHorizontal();

                                                    item.nomes[k] = GUILayout.TextField(item.nomes[k]);
                                                }
                                            }
                                            GUILayout.EndVertical();

                                            GUILayout.Space(5);

                                            item.iconeCatalogo = EditorGUILayout.TextField("Icone:", item.iconeCatalogo);

                                            GUILayout.Space(5);

                                            item.modelo3d = EditorGUILayout.TextField("Modelo 3D:", item.modelo3d);

                                            GUILayout.Space(5);

                                            if (_target.empresaId < 1)
                                            {
                                                EditorGUILayout.LabelField("Fabricantes");

                                                GUILayout.BeginVertical("box");
                                                {
                                                    if (GUILayout.Button("Adicionar Fabricante"))
                                                        item.fabricantesId.Add(-1);

                                                    for (int k = 0; k < item.fabricantesId.Count; k++)
                                                    {
                                                        item.fabricantesId[k] = EditorGUILayout.IntField("Fabricante " + k, item.fabricantesId[k]);
                                                    }

                                                    if (GUILayout.Button("Remover Fabricante"))
                                                        item.fabricantesId.RemoveAt(item.fabricantesId.Count - 1);
                                                }
                                                GUILayout.EndVertical();

                                                GUILayout.Space(5);
                                            }

                                            EditorGUILayout.LabelField("Descrição");

                                            GUILayout.BeginVertical("box");
                                            {
                                                for (int k = 0; k < DataController.NoIdiomas; k++)
                                                {
                                                    GUILayout.BeginHorizontal();
                                                    {
                                                        GUILayout.Label(DataController.Idiomas[k] + ":");

                                                        if (GUILayout.Button("C"))
                                                            EditorGUIUtility.systemCopyBuffer = item.descricao[k];
                                                        GUILayout.Space(5);
                                                        if (GUILayout.Button("P"))
                                                            item.descricao[k] = EditorGUIUtility.systemCopyBuffer;

                                                    }
                                                    GUILayout.EndHorizontal();

                                                    item.descricao[k] = GUILayout.TextArea(item.descricao[k], 1000,
                                                        GUILayout.Height(50), GUILayout.MaxWidth(EditorGUIUtility.currentViewWidth * .85F));
                                                }
                                            }
                                            GUILayout.EndVertical();

                                            GUILayout.Space(5);

                                            if (_target.empresaId > 0)
                                            {
                                                EditorGUILayout.LabelField("Videos");

                                                GUILayout.BeginVertical("box");
                                                {
                                                    if (GUILayout.Button("Add Video"))
                                                        item.videos.Add("nome_do_video");

                                                    for (int k = 0; k < item.videos.Count; k++)
                                                    {
                                                        item.videos[k] = EditorGUILayout.TextField("Video " + k, item.videos[k]);
                                                    }

                                                    if (GUILayout.Button("Remover Video"))
                                                        item.videos.RemoveAt(item.videos.Count - 1);
                                                }
                                                GUILayout.EndVertical();

                                                GUILayout.Space(5);
                                            }

                                            if (GUILayout.Button("Remove Item"))
                                            {
                                                data.itensCatalogo.RemoveAt(j);
                                                GUILayout.EndVertical();
                                                break;
                                            }
                                        }
                                        GUILayout.EndVertical();
                                    }
                                }

                            }
                            GUILayout.EndVertical();

                            GUI.backgroundColor = Color.white;
                        }

                        GUILayout.Space(5);

                        #endregion
                        #endregion
                    }
                    GUILayout.EndVertical();
                }
            }
            GUILayout.EndVertical();
            #endregion

            for (int x = 0; x < _target.Empresas.Count; x++)
            {
                var data = _target.Empresas[x];

                #region Empresas Específicas
                GUILayout.BeginVertical("box");
                {
                    data.geralNotFold = EditorGUILayout.Foldout(data.geralNotFold, "EMPRESA " + data.empresaId + " (" + data.empresaNome + "): ");

                    if (data.geralNotFold)
                    {
                        GUILayout.BeginVertical("box");
                        {
                            #region Dados Gerais
                            data.dadosNotFold = EditorGUILayout.Foldout(data.dadosNotFold, "Dados");

                            if (data.dadosNotFold)
                            {
                                GUILayout.Space(5);

                                data.empresaId = EditorGUILayout.IntField("Empresa Id: ", data.empresaId);

                                GUILayout.Space(5);

                                data.empresaNome = EditorGUILayout.TextField("Nome Empresa: ", data.empresaNome);

                                GUILayout.Space(5);

                                data.logo = EditorGUILayout.TextField("Logo: ", data.logo);

                                GUILayout.Space(5);

                                GUILayout.BeginVertical("box");
                                {
                                    GUILayout.BeginHorizontal();
                                    {
                                        GUILayout.Label("Site 1:");

                                        if (GUILayout.Button("C"))
                                            EditorGUIUtility.systemCopyBuffer = data.site1;
                                        GUILayout.Space(5);
                                        if (GUILayout.Button("P"))
                                            data.site1 = EditorGUIUtility.systemCopyBuffer;
                                    }
                                    GUILayout.EndHorizontal();

                                    data.site1 = EditorGUILayout.TextField(data.site1);

                                    GUILayout.BeginHorizontal();
                                    {
                                        GUILayout.Label("Site 2:");

                                        if (GUILayout.Button("C"))
                                            EditorGUIUtility.systemCopyBuffer = data.site2;
                                        GUILayout.Space(5);
                                        if (GUILayout.Button("P"))
                                            data.site2 = EditorGUIUtility.systemCopyBuffer;
                                    }
                                    GUILayout.EndHorizontal();

                                    data.site2 = EditorGUILayout.TextField(data.site2);
                                }
                                GUILayout.EndVertical();

                                GUILayout.Space(5);
                            }
                            #endregion
                            #region Itens Catálogo

                            data.itensNotFold = EditorGUILayout.Foldout(data.itensNotFold, "Itens");

                            if (data.itensNotFold)
                            {
                                GUI.backgroundColor = Color.cyan;

                                GUILayout.BeginVertical("box");
                                {
                                    if (GUILayout.Button("Add Item"))
                                        data.itensCatalogo.Add(new DataItemCatalogo(data.itensCatalogo.Count));

                                    for (int j = 0; j < data.itensCatalogo.Count; j++)
                                    {
                                        var item = data.itensCatalogo[j];

                                        item.notFold = EditorGUILayout.Foldout(item.notFold, item.id + " - " + item.nome);

                                        if (item.notFold)
                                        {
                                            GUILayout.BeginVertical("box");
                                            {
                                                GUILayout.Space(5);

                                                item.id = EditorGUILayout.IntField("Item Id:", item.id);

                                                GUILayout.Space(5);

                                                item.nome = EditorGUILayout.TextField("Nome Item:", item.nome);

                                                GUILayout.Space(5);

                                                if (item.nomes == null)
                                                {
                                                    item.nomes = new string[3] { item.nome, "", "" };
                                                }

                                                GUILayout.BeginVertical("box");
                                                {
                                                    for (int k = 0; k < DataController.NoIdiomas; k++)
                                                    {
                                                        GUILayout.BeginHorizontal();
                                                        {
                                                            GUILayout.Label(DataController.Idiomas[k] + ":");

                                                            if (GUILayout.Button("C"))
                                                                EditorGUIUtility.systemCopyBuffer = item.descricao[k];
                                                            GUILayout.Space(5);
                                                            if (GUILayout.Button("P"))
                                                            {
                                                                item.nomes[k] = EditorGUIUtility.systemCopyBuffer;
                                                                if (item.nomes[k].EndsWith("\n"))
                                                                {
                                                                    item.nomes[k] = item.nomes[k].Substring(0, item.nomes[k].Length - 1);
                                                                }
                                                            }

                                                        }
                                                        GUILayout.EndHorizontal();

                                                        item.nomes[k] = GUILayout.TextField(item.nomes[k]);
                                                    }
                                                }
                                                GUILayout.EndVertical();

                                                GUILayout.Space(5);

                                                item.iconeCatalogo = EditorGUILayout.TextField("Icone:", item.iconeCatalogo);

                                                GUILayout.Space(5);

                                                item.modelo3d = EditorGUILayout.TextField("Modelo 3D:", item.modelo3d);

                                                GUILayout.Space(5);

                                                if (data.empresaId < 1)
                                                {
                                                    EditorGUILayout.LabelField("Fabricantes");

                                                    GUILayout.BeginVertical("box");
                                                    {
                                                        if (GUILayout.Button("Adicionar Fabricante"))
                                                            item.fabricantesId.Add(-1);

                                                        for (int k = 0; k < item.fabricantesId.Count; k++)
                                                        {
                                                            item.fabricantesId[k] = EditorGUILayout.IntField("Fabricante " + k, item.fabricantesId[k]);
                                                        }

                                                        if (GUILayout.Button("Remover Fabricante"))
                                                            item.fabricantesId.RemoveAt(item.fabricantesId.Count - 1);
                                                    }
                                                    GUILayout.EndVertical();

                                                    GUILayout.Space(5);
                                                }

                                                EditorGUILayout.LabelField("Descrição");

                                                GUILayout.BeginVertical("box");
                                                {
                                                    for (int k = 0; k < DataController.NoIdiomas; k++)
                                                    {
                                                        GUILayout.BeginHorizontal();
                                                        {
                                                            GUILayout.Label(DataController.Idiomas[k] + ":");

                                                            if (GUILayout.Button("C"))
                                                                EditorGUIUtility.systemCopyBuffer = item.descricao[k];
                                                            GUILayout.Space(5);
                                                            if (GUILayout.Button("P"))
                                                                item.descricao[k] = EditorGUIUtility.systemCopyBuffer;

                                                        }
                                                        GUILayout.EndHorizontal();

                                                        item.descricao[k] = GUILayout.TextArea(item.descricao[k],
                                                            GUILayout.Height(50), GUILayout.MaxWidth(EditorGUIUtility.currentViewWidth * .85F));
                                                    }
                                                }
                                                GUILayout.EndVertical();

                                                GUILayout.Space(5);

                                                if (data.empresaId > 0)
                                                {
                                                    EditorGUILayout.LabelField("Videos");

                                                    GUILayout.BeginVertical("box");
                                                    {
                                                        if (GUILayout.Button("Add Video"))
                                                            item.videos.Add("nome_do_video");

                                                        for (int k = 0; k < item.videos.Count; k++)
                                                        {
                                                            item.videos[k] = EditorGUILayout.TextField("Video " + k, item.videos[k]);
                                                        }

                                                        if (GUILayout.Button("Remover Video"))
                                                            item.videos.RemoveAt(item.videos.Count - 1);
                                                    }
                                                    GUILayout.EndVertical();

                                                    GUILayout.Space(5);
                                                }

                                                if (GUILayout.Button("Remove Item"))
                                                {
                                                    data.itensCatalogo.RemoveAt(j);
                                                    GUILayout.EndVertical();
                                                    break;
                                                }
                                            }
                                            GUILayout.EndVertical();
                                        }
                                    }

                                }
                                GUILayout.EndVertical();

                                GUI.backgroundColor = Color.white;
                            }

                            GUILayout.Space(5);

                            #endregion
                        }
                        GUILayout.EndVertical();
                    }
                }
                GUILayout.EndVertical();
                #endregion
            }
        }

        
        
        _target.passeioNotFold = EditorGUILayout.Foldout(_target.passeioNotFold, "PASSEIOS: ");

        if (_target.passeioNotFold)
        {
            if (_target.passeios == null)
                _target.Load();

            #region Passeios
            GUI.backgroundColor = Color.green;
            GUILayout.BeginVertical("box");
            {
                if (GUILayout.Button("Add Item"))
                    _target.passeios.Add(new DataPasseio(DataController.NoIdiomas));

                for (int j = 0; j < _target.passeios.Count; j++)
                {
                    _target.passeios[j].notFoldOut = EditorGUILayout.Foldout(_target.passeios[j].notFoldOut, _target.passeios[j].Cena);

                    if (_target.passeios[j].notFoldOut)
                    {
                        _target.passeios[j].Cena = EditorGUILayout.TextField("Cena: ", _target.passeios[j].Cena);

                        for (int i = 0; i < DataController.NoIdiomas; i++)
                        {
                            _target.passeios[j].NomeSetor[i] = EditorGUILayout.TextField(DataController.Idiomas[i] + ":", _target.passeios[j].NomeSetor[i]);
                        }
                    }
                }
            }
            GUILayout.EndVertical();
            GUI.backgroundColor = Color.white;
                #endregion
        }
        

        _appNotFold = EditorGUILayout.Foldout(_appNotFold, "DADOS APP: ");

        if (_appNotFold)
        {
            #region Dados da aplicação
            GUI.backgroundColor = Color.yellow;

            GUILayout.BeginVertical("box");
            {
                var data = _target.DadosApp;

                GUILayout.Space(5);

                if (_editandoPalavra)
                {
                    GUILayout.BeginHorizontal();
                    {
                        if (GUILayout.Button("Cancelar"))
                        {
                            _editandoPalavra = false;
                        }
                        if (GUILayout.Button("Salvar"))
                        {
                            data.AddPalavra(_palavra, _traducoes);
                            _editandoPalavra = false;
                        }
                    }
                    GUILayout.EndHorizontal();
                }
                else
                {
                    if (GUILayout.Button("Nova Palavra"))
                    {
                        _editandoPalavra = true;
                        _palavra = "";
                        _traducoes = new string[DataController.NoIdiomas];

                        for (int i = 0; i < _palavrasFold.Count; i++)
                        {
                            _palavrasFold[i] = false;
                        }

                        for (int i = 0; i < DataController.NoIdiomas; i++)
                        {
                            _traducoes[i] = "";
                        }
                    }
                }

                if (_editandoPalavra)
                {
                    _palavra = EditorGUILayout.TextField("Nova Palavra:", _palavra);

                    for (int i = 0; i < _traducoes.Length; i++)
                    {
                        _traducoes[i] = EditorGUILayout.TextField(DataController.Idiomas[i] + ":", _traducoes[i]);
                    }
                }
                else
                {
                    if (_palavrasFold.Count != data.Palavras.Count)
                    {
                        _palavrasFold = new List<bool>();

                        for (int i = 0; i < _target.DadosApp.Palavras.Count; i++)
                        {
                            _palavrasFold.Add(false);
                        }
                    }

                    for (int i = 0; i < data.Palavras.Count; i++)
                    {
                        var estavaFechado = !_palavrasFold[i];

                        _palavrasFold[i] = EditorGUILayout.Foldout(_palavrasFold[i], data.Palavras[i]);

                        if (_palavrasFold[i])
                        {
                            if (estavaFechado)
                            {
                                for (int j = 0; j < data.Palavras.Count; j++)
                                {
                                    _palavrasFold[j] = false;
                                }

                                _palavrasFold[i] = true;

                                _traducoes = new string[DataController.NoIdiomas];

                                for (int j = 0; j < DataController.NoIdiomas; j++)
                                {
                                    _traducoes[j] = data.Traduzir(i, j);
                                }
                            }

                            for (int j = 0; j < _traducoes.Length; j++)
                            {
                                EditorGUILayout.LabelField(DataController.Idiomas[j] + ":" + _traducoes[j]);
                            }
                        }
                    }
                }
            }
            GUILayout.EndVertical();

            GUI.backgroundColor = Color.white;
            #endregion
        }

        EditorUtility.SetDirty(_target);
    }
}
