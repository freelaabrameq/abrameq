﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

public class GameController : MonoBehaviour {

    private static GameController _instance;

    private TelaController _telaController;
    private DataController _dataController;

    public static bool wasInitialized;

    public static bool orientationDefined;

    public static bool voltandoDoPasseio;

    public static int pularParaMaquinaId;

    public static int passeioAtual;

    void Awake()
    {
        GetInstance();
        GetDataController().Init();

        if (GetTelaController())
            GetTelaController().Init();

        if (voltandoDoPasseio)
        {
            voltandoDoPasseio = false;

#if UNITY_STANDALONE || UNITY_EDITOR
            Screen.SetResolution(480, 800, false);
#else
            Screen.orientation = ScreenOrientation.Portrait;
#endif

            GetTelaController().txtNomeEmpresa.text = GetDataController().DadosEmpresa.empresaNome;

            foreach (var item in GetTelaController().buttonVoltar.GetComponentsInChildren<Text>(true))
            {
                item.text = GetDataController().DadosApp.Traduzir("Voltar");  
            }

            //GetTelaController().buttonVoltar.GetComponentInChildren<UnityEngine.UI.Text>().text = GetDataController().DadosApp.Traduzir("Voltar");  

            if (pularParaMaquinaId > -1)
            {
                TelaController.TelaAtualId = (int)Telas.TelaItemCatalogo;

                GetTelaController().buttonVoltar.onClick = new Button.ButtonClickedEvent();
                GetTelaController().buttonVoltar.onClick.AddListener(delegate { GetTelaController().GetComponentInChildren<TelaPasseio>().PasseioEscolhido(passeioAtual); });

                GetDataController().SetaItemCatalogo(pularParaMaquinaId);
            }
            else
            {
                if (GetDataController().passeios.Count(x => !string.IsNullOrEmpty(x.Cena)) > 1)
                    TelaController.TelaAtualId = (int)Telas.TelaPasseio;
                else
                    TelaController.TelaAtualId = (int)Telas.TelaMenu;
            }
        }


        pularParaMaquinaId = -1;
    }
    public DataController GetDataController()
    {
        if (_dataController == null)
        {
            _dataController = GetInstance().GetComponentInChildren<DataController>();
        }

        return _dataController;
    }

    public TelaController GetTelaController()
    {
        if (_telaController == null)
        {
            _telaController = GetInstance().GetComponentInChildren<TelaController>();
        }

        return _telaController;
    }

    public static GameController GetInstance()
    {
        if (_instance == null)
        {
            _instance = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        }
        return _instance;
    }


}
