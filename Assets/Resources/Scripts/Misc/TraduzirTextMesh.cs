﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class TraduzirTextMesh : MonoBehaviour
{

    private TextMesh _myText;

    // Use this for initialization
    void Start()
    {
        var dataController = GameController.GetInstance().GetDataController();

        _myText = this.GetComponent<TextMesh>();

        var texto = _myText.text;

        if (texto.Contains("#"))
        {
            var substrings = texto.Split('#');

            texto = substrings[0];

            for (int i = 1; i < substrings.Length; i++)
            {
                var miniStrings = substrings[i].Split(' ');

                var index = -1;

                if (int.TryParse(miniStrings[0], out index))
                {
                    miniStrings[0] = dataController.DadosEmpresa.itensCatalogo.First(x => x.id == index).nomes[DataController.IdiomaId];
                }

                for (int j = 0; j < miniStrings.Length; j++)
                {
                    texto += " " + miniStrings[j];
                }
            }
        }

        if (texto.Contains("$"))
        {
            var substrings = texto.Split('$');

            texto = substrings[0];

            for (int i = 1; i < substrings.Length; i++)
            {
                var miniStrings = substrings[i].Split(' ');

                miniStrings[0] = dataController.DadosApp.Traduzir("$" + miniStrings[0]);

                for (int j = 0; j < miniStrings.Length; j++)
                {
                    texto += " " + miniStrings[j];
                }
            }
        }

        _myText.text = texto;

        if (_myText.text.Contains("\\n"))
            _myText.text = _myText.text.Replace("\\n", "\n");
    }

}
