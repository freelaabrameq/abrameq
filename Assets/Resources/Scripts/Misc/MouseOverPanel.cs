﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class MouseOverPanel : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    public TelaModeloItem telaModeloItem;

    #region IPointerExitHandler Members

    public void OnPointerExit(PointerEventData eventData)
    {
        telaModeloItem.mouseOverPanel = false;
    }

    #endregion

    #region IPointerEnterHandler Members

    public void OnPointerEnter(PointerEventData eventData)
    {
        telaModeloItem.mouseOverPanel = true;
    }

    #endregion
}
