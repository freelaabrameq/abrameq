﻿using UnityEngine;

public class HideOnFactory : MonoBehaviour
{
    public int factoryIdToHideFrom;

    void Awake()
    {
        this.gameObject.SetActive(DataController.DadosEmpresaAtual.empresaId != factoryIdToHideFrom);
    }
}
