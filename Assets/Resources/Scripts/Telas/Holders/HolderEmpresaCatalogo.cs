﻿using UnityEngine;
using System.Collections;

public class HolderEmpresaCatalogo : HolderTelaBase
{

    public RectTransform cartaoBase;

    [HideInInspector]
    public RectTransform[] cartoesEmpresa;

    public RectTransform rtHolderCartoes;

}
