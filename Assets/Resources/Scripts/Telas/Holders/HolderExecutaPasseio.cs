﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HolderExecutaPasseio : HolderTelaBase
{
    public Button btPause, btInfo, btVoltar;

    public Image imgBtPause;

    public Text txtTituloTela, txtNomeEmpresa;

    public CanvasScaler canvasScaler;

    public RectTransform rtUpperLargo, rtUpper;

    [HideInInspector]
    public RectTransform rtCanvas;
}
