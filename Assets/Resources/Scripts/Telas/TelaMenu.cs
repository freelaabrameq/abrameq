﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TelaMenu : TelaBase
{
    private HolderMenu menuUI;

    public HolderItensIntro itensIntro;

    void Awake()
    {
        Init();
    }

    public override void Init()
    {
        telaTipo = Telas.TelaMenu;
        telaAnterior = Telas.TelaAbertura;

        base.Init();

        base.Open();

        menuUI = Holder.GetComponent<HolderMenu>();
        menuUI.Init();

        base.Close();
	}
    void Start()
    {
        if (_wasInitialized)
            return;

        _wasInitialized = true;

        itensIntro.txtNomeDaEmpresa.text = _dataController.DadosEmpresa.empresaNome;

        itensIntro.imgLogo.sprite = _dataController.PegaLogo();

        itensIntro.txtLink1.text = _dataController.DadosEmpresa.site1;
        itensIntro.txtLink2.text = _dataController.DadosEmpresa.site2;

        #region Menu
        menuUI.btCatalogo.onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
        menuUI.btCatalogo.onClick.AddListener(delegate { _telaController.MudaTela((int)Telas.TelaCatalogo); });

        menuUI.btPasseio.onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
        menuUI.btPasseio.onClick.AddListener(delegate { _telaController.MudaTela((int)Telas.TelaPasseio); });
        #endregion

        SetaIdioma();
    }
    public override void Open()
    {
        base.Open();

        itensIntro.holder.SetActive(true);

        SetaIdioma();
    }
    private void SetaIdioma()
    {
        menuUI.txtBtCatalogo.text = _dataController.DadosApp.Traduzir("Catálogo");
        menuUI.txtBtPasseio.text = _dataController.DadosApp.Traduzir("Passeio Virtual");

        itensIntro.txtRealizacao.text = _dataController.DadosApp.Traduzir("Realização");
        itensIntro.txtApoio.text = _dataController.DadosApp.Traduzir("Apoio");
    }
    public override void Close()
    {

        itensIntro.holder.SetActive(false);

        base.Close();
    }
}
