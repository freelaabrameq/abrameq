﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TelaExecutaPasseio : TelaBase
{
    private HolderExecutaPasseio passeioUI;

    public Sprite sptPlay, sptPause;

    public int passeioID;

    public CameraPath cameraPath;

    private int _maquinaAtualId;

    private Vector3 _PosCamera;

    private bool _estaOrbitando;

    private float _height;

    void Awake()
    {
        Init();
#if UNITY_STANDALONE || UNITY_EDITOR
        Screen.SetResolution(800, 480, false);
#else
        Screen.orientation = ScreenOrientation.Landscape;
#endif
    }
    void Start()
    {
        if (_wasInitialized)
            return;

        _wasInitialized = true;

        passeioUI.btPause.onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
        passeioUI.btPause.onClick.AddListener(delegate { this.EnableOrbit(); });

        passeioUI.btInfo.onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
        passeioUI.btInfo.onClick.AddListener(delegate { this.ButtonInfo(); });

        passeioUI.btVoltar.onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
        passeioUI.btVoltar.onClick.AddListener(delegate { this.Close(); });
    }
    public override void Init()
    {
        telaTipo = Telas.TelaExecutaPasseio;
        telaAnterior = Telas.TelaPasseio;

        //base.Init();

        this._dataController = GameController.GetInstance().GetDataController();

        base.Open();

        passeioUI = Holder.GetComponent<HolderExecutaPasseio>();
        passeioUI.Init();

        passeioUI.rtCanvas = passeioUI.canvasScaler.GetComponent<RectTransform>();

        Open();
    }
    public void EnableOrbit()
    {
        if (_estaOrbitando)
        {
            cameraPath.aux = _PosCamera;
            cameraPath.trnCamera.position = _PosCamera;
        }
        else
        {
            _PosCamera = cameraPath.trnCamera.position;
        }

        _estaOrbitando = cameraPath.Orbit(cameraPath.bezierPoint[cameraPath.currentPath].blockOrbit);

        passeioUI.imgBtPause.sprite = _estaOrbitando ? sptPlay : sptPause;

        AudioListener.volume = _estaOrbitando ? 0 : 1;
    }
    public void ButtonInfo()
    {
        if (cameraPath.bezierPoint[cameraPath.currentPath].id > -1)
        {
            GameController.voltandoDoPasseio = true;
            GameController.pularParaMaquinaId = _maquinaAtualId;

            cameraPath.SaveState();

            Application.LoadLevel(0);
        }
    }
    public void EnableInfo(int maquinaAtual)
    {
        if (maquinaAtual > -1)
        {
            if (DataController.DadosEmpresaAtual.itensCatalogo.Exists(x => x.id == maquinaAtual))
            {
                _maquinaAtualId = maquinaAtual;
                passeioUI.btInfo.interactable = true;
            }
            else
            {
                passeioUI.btInfo.interactable = false;
            }
        }
        else
        {
            passeioUI.btInfo.interactable = false;
        }

    }
    public void EnablePause(bool enabled)
    {
        passeioUI.btPause.interactable = enabled;
    }
    public override void Open()
    {
        passeioUI.imgBtPause.sprite = sptPause;

        cameraPath.Init(this);

        passeioUI.btVoltar.GetComponentInChildren<Text>().text = _dataController.DadosApp.Traduzir("Sair");
        passeioUI.txtNomeEmpresa.text = DataController.DadosEmpresaAtual.empresaNome;
        passeioUI.txtTituloTela.text = _dataController.passeios[passeioID].NomeSetor[DataController.IdiomaId];
    }
    void Update()
    {
        var heightN = passeioUI.rtCanvas.sizeDelta.y;

        if (_height != heightN)
        {
            _height = heightN;

            passeioUI.rtUpper.anchoredPosition = Vector2.up * (_height - passeioUI.canvasScaler.referenceResolution.y) / 2;
            passeioUI.rtUpperLargo.anchoredPosition = passeioUI.rtUpper.anchoredPosition;
        }

    }
    public override void Close()
    {
        base.Close();
        
        GameController.pularParaMaquinaId = -1;
        GameController.voltandoDoPasseio = true;

        cameraPath.ClearState();

        Application.LoadLevel(0);
    }
}
