﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TelaPasseio : TelaBase
{
    private HolderPasseio passeioUI;
    private bool _showOptions;
    void Awake()
    {
        Init();
    }

    public override void Init()
    {
        telaTipo = Telas.TelaPasseio;
        telaAnterior = Telas.TelaMenu;

        base.Init();

        base.Open();

        passeioUI = Holder.GetComponent<HolderPasseio>();
        passeioUI.Init();

        passeioUI.rtCarregando.gameObject.SetActive(false);

        base.Close();
    }

    void Start()
    {
        if (_wasInitialized)
            return;

        _wasInitialized = true;

        this.Open();

        var buttonsCount = _dataController.passeios.Count;

        passeioUI.buttons = new Button[buttonsCount];

        var containerParentSize = passeioUI.rtContainer.transform.parent.GetComponent<RectTransform>().sizeDelta;

        passeioUI.rtContainer.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (1 + buttonsCount) * 60 + 20);
        passeioUI.rtContainer.transform.localPosition = Vector3.up * (containerParentSize.y - passeioUI.rtContainer.sizeDelta.y) / 2;

        if (containerParentSize.y > passeioUI.rtContainer.sizeDelta.y)
        {
            passeioUI.rtContainer.transform.parent.GetComponent<ScrollRect>().enabled = false;
        }

        var pos = new Vector3(0, passeioUI.rtContainer.sizeDelta.y / 2 - 20, 0);

        var options = 0;

        for (int i = 0; i < _dataController.passeios.Count; i++)
        {
            passeioUI.buttons[i] = Instantiate(passeioUI.btReferenciaPasseio);
            passeioUI.buttons[i].transform.SetParent(passeioUI.rtContainer.transform);
            passeioUI.buttons[i].transform.localScale = passeioUI.btReferenciaPasseio.transform.localScale;
            passeioUI.buttons[i].name = "ButtonPasseio" + i;
            passeioUI.buttons[i].GetComponentInChildren<Text>().text = _dataController.passeios[i].Cena;

            passeioUI.buttons[i].interactable = !string.IsNullOrEmpty(_dataController.passeios[i].Cena);

            var id = i;

            passeioUI.buttons[i].onClick = new Button.ButtonClickedEvent();
            passeioUI.buttons[i].onClick.AddListener(delegate { this.PasseioEscolhido(id); });

            passeioUI.buttons[i].transform.localPosition = pos;
            passeioUI.buttons[i].transform.localPosition -= Vector3.up * 60 * i;


            if (passeioUI.buttons[i].interactable)
                options++;
            else
                passeioUI.buttons[i].gameObject.SetActive(false);
        }

        _showOptions = options > 1;

        Destroy(passeioUI.btReferenciaPasseio.gameObject);

        this.Close();
    }
    public void PasseioEscolhido(int id)
    {
        passeioUI.rtCarregando.gameObject.SetActive(true);

        passeioUI.txtCarregando.text = _dataController.DadosApp.Traduzir("Carregando") + "...";

        GameController.passeioAtual = id;

        Application.LoadLevel(_dataController.passeios[id].Cena);

        //_dataController.SetaPasseio(id);
        //_telaController.MudaTela(Telas.TelaExecutaPasseio);
    }
    public override void Open()
    {
        base.Open();

        if (passeioUI)
        {
            _telaController.txtTituloTela.text = _dataController.DadosApp.Traduzir("Passeio Virtual");
            passeioUI.txtFrase.text = _dataController.DadosApp.Traduzir("EscolhaSetor");

            if (_showOptions)
            {
                for (int i = 0; i < passeioUI.buttons.Length; i++)
                {
                    if (passeioUI.buttons[i].interactable)
                        passeioUI.buttons[i].GetComponentInChildren<Text>().text = _dataController.passeios[i].NomeSetor[DataController.IdiomaId];
                }
            }
            else
            {
                for (int i = 0; i < passeioUI.buttons.Length; i++)
                {
                    if (passeioUI.buttons[i].interactable)
                        this.PasseioEscolhido(i);
                }
            }
        }
    }
}
