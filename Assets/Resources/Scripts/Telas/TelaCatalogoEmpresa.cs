﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class TelaCatalogoEmpresa : TelaBase
{
    private HolderEmpresaCatalogo empresaUI;

    public RectTransform[] cartoesBase;

    void Awake()
    {
        Init();
    }

    public override void Init()
    {
        telaTipo = Telas.TelaFabricantes;
        telaAnterior = Telas.TelaItemCatalogo;

        base.Init();

        base.Open();

        empresaUI = Holder.GetComponent<HolderEmpresaCatalogo>();
        empresaUI.Init();

        var lista = new List<RectTransform>();

        lista.AddRange(Resources.LoadAll<RectTransform>("ArtsContents/Prefabs/CartoesFabricantes/"));

        cartoesBase = lista.OrderBy(x => x.name).ToArray();

        base.Close();
    }

    public override void Open()
    {
        base.Open();

        if (empresaUI)
        {
            empresaUI.cartaoBase.gameObject.SetActive(true);

            var cartoes = _dataController.ItemCatalogoAtual.fabricantesId.Count;
            var size = empresaUI.cartaoBase.GetComponent<RectTransform>().sizeDelta;

            if (empresaUI.cartoesEmpresa != null)
            {
                for (int i = 0; i < empresaUI.cartoesEmpresa.Length; i++)
                {
                    Destroy(empresaUI.cartoesEmpresa[i].gameObject);
                }
            }

            empresaUI.cartoesEmpresa = new RectTransform[cartoes];

            for (int i = 0; i < cartoes; i++)
            {
                var cartaoId = _dataController.ItemCatalogoAtual.fabricantesId[i] - 1;

                empresaUI.cartoesEmpresa[i] = cartaoId > -1 ? Instantiate(cartoesBase[cartaoId]) : Instantiate(empresaUI.cartaoBase);
                empresaUI.cartoesEmpresa[i].transform.SetParent(empresaUI.rtHolderCartoes.transform);
                empresaUI.cartoesEmpresa[i].transform.localScale = empresaUI.cartaoBase.transform.localScale;

                empresaUI.cartoesEmpresa[i].transform.localPosition = empresaUI.cartaoBase.transform.localPosition;
                empresaUI.cartoesEmpresa[i].transform.localPosition -= Vector3.up * (size.y + 20) * i;
            }

            empresaUI.rtHolderCartoes.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, _telaController.rtCanvas.sizeDelta.y);

            var pos = Vector2.zero;

            if (cartoes > 0)
                pos = empresaUI.cartoesEmpresa[cartoes - 1].GetComponent<RectTransform>().anchoredPosition;

            pos.y = Mathf.Abs(pos.y);

            if (pos.y + size.y > _telaController.rtCanvas.sizeDelta.y - _telaController.rtBottom.sizeDelta.y)
            {
                var dif = pos.y + size.y - (_telaController.rtCanvas.sizeDelta.y - _telaController.rtBottom.sizeDelta.y);

                empresaUI.rtHolderCartoes.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, empresaUI.rtHolderCartoes.sizeDelta.y + dif);
            }

            pos.y = -(empresaUI.rtHolderCartoes.sizeDelta.y / 2);

            empresaUI.rtHolderCartoes.anchoredPosition = pos;

            empresaUI.cartaoBase.gameObject.SetActive(false);

            //Criar cartões de acordo com a empresa
        }
    }
}
