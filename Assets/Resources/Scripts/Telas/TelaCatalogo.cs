﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

public class TelaCatalogo : TelaBase
{
    private HolderCatalogo catalogoUI;

    void Awake()
    {
        Init();
    }

    public override void Init()
    {
        telaTipo = Telas.TelaCatalogo;
        telaAnterior = Telas.TelaMenu;

        base.Init();

        base.Open();

        catalogoUI = Holder.GetComponent<HolderCatalogo>();
        catalogoUI.Init();

        base.Close();
	}

    public override void Open()
    {
        base.Open();

        if (catalogoUI)
        {
            _telaController.txtTituloTela.text = _dataController.DadosApp.Traduzir("Catálogo").ToUpper();

            var itensOrganizados = _dataController.DadosEmpresa.itensCatalogo.OrderBy(x => x.nome).ToArray();

            for (int i = 0; i < catalogoUI.buttons.Length; i++)
            {
                catalogoUI.buttons[i].GetComponentInChildren<Text>().text = itensOrganizados[i].nomes[DataController.IdiomaId];
            }
            
        }
    }

    void Start()
    {
        if (_wasInitialized)
            return;

        base.Open();

        _wasInitialized = true;

        var buttonsCount = _dataController.DadosEmpresa.itensCatalogo.Count;

        catalogoUI.buttons = new Button[buttonsCount];

//        var containerParentSize = catalogoUI.rtContainer.transform.parent.GetComponent<RectTransform>().sizeDelta;

        var btSize = catalogoUI.btItemReference.GetComponent<RectTransform>().sizeDelta;

        catalogoUI.rtContainer.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, btSize.y * (buttonsCount % 2 > 0 ? (buttonsCount + 1) / 2F : buttonsCount / 2F));
        catalogoUI.rtContainer.anchoredPosition = new Vector2(0, 0);

        btSize.x /= 2;

        var itensOrganizados = _dataController.DadosEmpresa.itensCatalogo.OrderBy(x => x.nome).ToArray();


        var esquerda = true;
        var posY = 0F;

        for (int i = 0; i < buttonsCount; i++)
        {
            catalogoUI.buttons[i] = Instantiate(catalogoUI.btItemReference);
            catalogoUI.buttons[i].transform.SetParent(catalogoUI.rtContainer.transform);
            catalogoUI.buttons[i].transform.localScale = catalogoUI.btItemReference.transform.localScale;
            catalogoUI.buttons[i].name = "ButtonItem" + itensOrganizados[i].id;
            catalogoUI.buttons[i].targetGraphic.GetComponent<Image>().sprite = _dataController.PegaImagemCatalogo(itensOrganizados[i].id);

            var id = itensOrganizados[i].id;

            catalogoUI.buttons[i].onClick = new Button.ButtonClickedEvent();
            catalogoUI.buttons[i].onClick.AddListener(delegate { this.ButtonItemEscolhido(id); });

            var rt = catalogoUI.buttons[i].GetComponent<RectTransform>();

            rt.anchoredPosition = new Vector2(esquerda ? -btSize.x : btSize.x, posY);

            if (esquerda)
            {
                esquerda = false;
            }
            else
            {
                esquerda = true;
                posY -= btSize.y;
            }
        }

        Destroy(catalogoUI.btItemReference.gameObject);

        base.Close();
    }

    public void ButtonItemEscolhido(int itemId)
    {
        _dataController.SetaItemCatalogo(itemId);
        _telaController.MudaTela(Telas.TelaItemCatalogo);
    }

}
