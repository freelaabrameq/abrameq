﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TelaVideosItemCatalogo : TelaBase
{
    private HolderVideoItemCatalogo videoItemUI;

    private bool _singleVideo;

    void Awake()
    {
        Init();
    }

    public override void Init()
    {
        telaTipo = Telas.TelaVideosItem;
        telaAnterior = Telas.TelaItemCatalogo;

        base.Init();

        base.Open();

        videoItemUI = Holder.GetComponent<HolderVideoItemCatalogo>();
        videoItemUI.Init();

        base.Close();
    }
    public void VideoEscolhido(int id)
    {
#if UNITY_ANDROID || UNITY_IOS
        StartCoroutine(PlayVideo(id));
#else
        videoItemUI.goPanel.SetActive(false);
#endif
    }
    IEnumerator PlayVideo(int id)
    {
        videoItemUI.blackScreen.SetActive(true);

        yield return new WaitForSeconds(1);

        Screen.orientation = ScreenOrientation.Landscape;

        yield return new WaitForSeconds(1);

        Handheld.PlayFullScreenMovie(_dataController.ItemCatalogoAtual.videos[id] + ".mp4", Color.black, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.AspectFit);

        yield return new WaitForSeconds(1);

        Screen.orientation = ScreenOrientation.Portrait;

        if (_singleVideo)
            _telaController.MudaTela(telaAnterior);

        videoItemUI.blackScreen.SetActive(false);

        yield return new WaitForSeconds(1);


    }
    void Update()
    {
#if UNITY_STANDALONE || UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            videoItemUI.goPanel.SetActive(true);
        }
#endif
    }

    public override void Open()
    {
        base.Open();

        if (videoItemUI)
        {
            _telaController.txtTituloTela.text = _dataController.DadosApp.Traduzir("Vídeos") + " | " + _dataController.ItemCatalogoAtual.nomes[DataController.IdiomaId];

            if (_dataController.ItemCatalogoAtual.videos.Count > 1)
            {
                #region Criar botões
                var buttonsCount = _dataController.ItemCatalogoAtual.videos.Count;

                if (videoItemUI.buttons != null)
                {
                    for (int i = 0; i < videoItemUI.buttons.Length; i++)
                    {
                        Destroy(videoItemUI.buttons[i].gameObject);
                    }
                }

                videoItemUI.buttons = new Button[buttonsCount];

                if ((buttonsCount * 230 + 10) > videoItemUI.rtContainer.sizeDelta.y)
                {
                    videoItemUI.rtContainer.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, buttonsCount * 230 + 10);
                    videoItemUI.rtContainer.anchoredPosition = Vector2.up;
                }

                videoItemUI.btReferencia.gameObject.SetActive(true);

                for (int i = 0; i < _dataController.ItemCatalogoAtual.videos.Count; i++)
                {
                    videoItemUI.buttons[i] = Instantiate(videoItemUI.btReferencia);
                    videoItemUI.buttons[i].transform.SetParent(videoItemUI.rtContainer.transform);
                    videoItemUI.buttons[i].transform.localScale = videoItemUI.btReferencia.transform.localScale;
                    videoItemUI.buttons[i].name = "ButtonVideo" + i;
                    videoItemUI.buttons[i].targetGraphic.GetComponent<Image>().sprite = Resources.Load("ArtsContents/Texture/ImagensVideos/" + _dataController.ItemCatalogoAtual.videos[i], typeof(Sprite)) as Sprite;

                    var id = i;

                    videoItemUI.buttons[i].onClick = new Button.ButtonClickedEvent();
                    videoItemUI.buttons[i].onClick.AddListener(delegate { this.VideoEscolhido(id); });

                    videoItemUI.buttons[i].GetComponent<RectTransform>().anchoredPosition = Vector2.up * 10 - Vector2.up * 230 * i;
                }

                videoItemUI.btReferencia.gameObject.SetActive(false);
                #endregion
            }
            else
            {
                _singleVideo = true;

                this.VideoEscolhido(0);
            }
        }
    }
}
