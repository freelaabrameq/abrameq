﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum PasseioType
{
    SCN_Curtimento,
    SCN_Semi_Acabado,
    SCN_Acabamento,
    SCN_TratamentoEfluentes
}