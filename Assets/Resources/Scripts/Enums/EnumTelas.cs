﻿using UnityEngine;
using System.Collections;

public enum Telas
{
    none = -1,
    TelaAbertura,
    TelaMenu,
    TelaCatalogo,
    TelaItemCatalogo,
    TelaVideosItem,
    TelaPasseio,
    TelaExecutaPasseio,
    TelaModeloItem,
    TelaFabricantes,
    todas
}
