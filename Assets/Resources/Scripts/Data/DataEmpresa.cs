﻿using UnityEngine;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;

using System.Linq;


[System.Serializable]
public class DataEmpresa {

    public int empresaId;

    public string empresaNome, logo;

    public string site1, site2;

    public List<DataItemCatalogo> itensCatalogo;

    public List<PasseioType> passeios;


    [XmlIgnore]
    public bool dadosNotFold, itensNotFold, geralNotFold, passeiosNotFold;

    //public void SetaIdioma(int id)
    //{
    //    if (idiomasHabilitados[id])
    //    {
    //        for (int i = 0; i < itensCatalogo.Count; i++)
    //        {
    //            itensCatalogo[i].descricaoAtual = itensCatalogo[i].descricao[id];
    //        }
    //    }
    //}

    public DataEmpresa() { }

    public DataEmpresa(int id)
    {
        this.empresaId = id;
        this.empresaNome = "Nome da Empresa";
        this.site1 = "www.site.com.br";
        this.site2 = "www.outro.com.br";

        this.itensCatalogo = new List<DataItemCatalogo>();
    }
    
    


}
