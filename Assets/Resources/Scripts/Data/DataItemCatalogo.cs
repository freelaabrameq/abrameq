﻿using UnityEngine;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;

using System;

[Serializable]
public class DataItemCatalogo
{
    public int id;

    public string nome;

    public string[] nomes;

    public string iconeCatalogo;

    public string modelo3d;

    public string link;

    public string[] descricao;

    public List<int> fabricantesId;

    //[XmlIgnore]
    //public string descricaoAtual;

    public List<string> videos;

    [XmlIgnore]
    public bool notFold;

    public DataItemCatalogo() { }

    public DataItemCatalogo(int id)
    {
        this.id = id;

        this.nome = "Nome Item";

        this.nomes = new string[DataController.NoIdiomas];

        this.iconeCatalogo = "nome_icone";

        this.modelo3d = "nome_modelo3d";

        this.descricao = new string[DataController.NoIdiomas];

        this.videos = new List<string>();

        this.notFold = true;

        this.fabricantesId = new List<int>();

        for (int i = 0; i < DataController.NoIdiomas; i++)
        {
            this.descricao[i] = "";
        }
    }

}
